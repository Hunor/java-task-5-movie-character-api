package com.noroff.moviecharacterapi.controllers;

import com.noroff.moviecharacterapi.models.Character;
import com.noroff.moviecharacterapi.models.Movie;
import com.noroff.moviecharacterapi.repositories.CharacterRepository;
import com.noroff.moviecharacterapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;


    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies,status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id){
        Movie returnMovie = new Movie();
        HttpStatus status;
        // We first check if the Book exists, this saves some computing time.
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        HttpStatus status;
        movie = movieRepository.save(movie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        if(!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie,status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    @GetMapping("{id}/characters")
    public ResponseEntity<List<Character>> getCharactersInMovie(@PathVariable Long id){
        Movie returnMovieCharacters = new Movie();
        HttpStatus status;
        if(characterRepository.existsById(id)){
            status = HttpStatus.OK;
            returnMovieCharacters = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovieCharacters.getCharacters(), status);
    }

    @DeleteMapping ("/{id}")
    public ResponseEntity deleteMovieById(@PathVariable Long id){
        Movie returnMovie = new Movie();
        HttpStatus status;
        // We first check if the Book exists, this saves some computing time.
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            movieRepository.deleteById(id);
        } else {
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(returnMovie, status);
    }
}
