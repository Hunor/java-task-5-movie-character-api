package com.noroff.moviecharacterapi.repositories;

import com.noroff.moviecharacterapi.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie,Long> {

}
