package com.noroff.moviecharacterapi.repositories;

import com.noroff.moviecharacterapi.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Long> {
}
