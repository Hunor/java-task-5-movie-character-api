package com.noroff.moviecharacterapi.repositories;

import com.noroff.moviecharacterapi.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise,Long> {
}
