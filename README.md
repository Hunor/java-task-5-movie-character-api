# API for Franchises, Movies and Characters

This is a REST API made with Java Spring (Spring Boot, Spring Data, Hibernate and JPA). The database is 
a PostgreSQL DB, with tables for franchises, movies and characters. There are multiple franchises, 
multiple movies in a franchise and multiple characters in a movie and in a franchise. The API has been 
created with best practises in mind.

Link to Heroku: https://movie-character-api-task5.herokuapp.com

A collection of API calls in Postman: [Link API collections](https://gitlab.com/Hunor/java-task-5-movie-character-api/-/blob/master/APICollectionPostman.postman_collection.json)

## Main specifications

Entities are created for the task and their relationships with Spring Data and Hibernate. The database
is published in Heroku and the application has been configured to tie into the published database as well
as being published in Heroku.

## API calls

- Generec full CRUD (Create, Read, Update and Delete) for every table to manage entities:
  * Franchise
    * Id, Name, Description
  * Movie
    * Id, Title, Genre, Release year, Director, Image, Trailer
  * Character
    * Id, Name, Alias, Gender, Image
- Additional API reporting:
  * Get all the movies in a franchise
  * Get all characters in a movie
  * Get all the characters in a franchise
- A collection of API calls with Postman is included in the repo

## Documentation

|Endpoint|HTTP Method| 
|---|---|
|/api/v1/franchises|GET/POST|
|/api/v1/movies|GET/POST|
|/api/v1/characters|GET/POST|
|/api/v1/franchises/{id}|GET/PUT/DELETE|
|/api/v1/movies/{id}|GET/PUT/DELETE|
|/api/v1/characters/{id}|GET/PUT/DELETE|
|/api/v1/franchises/{id}/movies|GET|
|/api/v1/franchises/{id}/characters|GET|
|/api/v1/movies/{id}/characters|GET|


## Project participants
- Hunor Vadasz-Perhat
- Jesse Saarimaa
- Mikko Siukola
